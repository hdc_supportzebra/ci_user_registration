$("input:checkbox").click(function() {

	if ($(this).is(":checked")) {

		var group = "input:checkbox[name='" + $(this).attr("name") + "']";
		
		$(group).prop("checked", false);
		$(this).prop("checked", true);
	} 
	else {
		$(this).prop("checked", false);
	}
});


var FormValidation = function () {

    var addUser = function () {
        $( "#add_user_form" ).validate({
            // define validation rules
            rules: {
            	firstname: {
            		required: true,
            		lettersonly: true,
            	},
				lastname: {
            		required: true,
            		lettersonly: true,
            	},            	
                // email: {
                //     required: true,
                //     email: true 
                // },
                username: {
                    required: true,
                    minlength: 8
                },
                password: {
                    required: true,
                    minlength: 8
                },
                usertype: {
                    required: true,
                    minlength: 1,
                },
                status: {
                    required: true,
                    minlength: 1,
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                mApp.scrollTo("#add_user_form");

                swal({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            submitHandler: function (form) {

                var firstname = $("input[name='firstname']").val();
                var lastname = $("input[name='lastname']").val();
                var username = $("input[name='username']").val();
                var password = $("input[name='password']").val();
                var usertype = $(':checkbox:checked').val();
                var status = $("select[name='status']").val();

                var options = { 
                    url: $(this).attr('action'),
                    type: 'POST',
                    dataType: "json",
                    data: {firstname:firstname, lastname:lastname, username:username, password:password, usertype:usertype, status:status},
                    success:  function(response) { 
                        if(response.status === true)
                        {
                           swal({
                                "title": "", 
                                "text": "Successfully added user.", 
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false

                            });

                           window.setTimeout(function(){
                            document.location.href = response.redirect;

                        }, 1800);


                        }
                    },
                    error:  function() { 
                        alert('error'); 
                    },
                }; 
              $(form).ajaxSubmit(options);
            }
        });       
    }

    var editPersonalInfo = function() {
        $( "#edit_personal_info" ).validate({
            // define validation rules
            rules: {
                firstname: {
                    required: true,
                    lettersonly: true,
                },
                lastname: {
                    required: true,
                    lettersonly: true,
                }        
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                mApp.scrollTo("#edit_personal_info");

                swal({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            submitHandler: function (form) {

                var user_id = $("input[name='user_id']").val();
                var firstname = $("input[name='firstname']").val();
                var lastname = $("input[name='lastname']").val();

                var options = { 
                    urll: $(this).attr('action'),
                    type: 'POST',
                    dataType: "json",
                    data: {user_id:user_id, firstname:firstname, lastname:lastname},
                    success:  function(response) { 
                        if(response.status === true)
                        {
                           swal({
                                "title": "", 
                                "text": "Successfully updated user.", 
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false

                            });

                        //    window.setTimeout(function(){
                        //     document.location.href = response.redirect;

                        // }, 1800);


                        }
                    },
                    error:  function() { 
                        alert('error'); 
                    },
                }; 
              $(form).ajaxSubmit(options);
            }
        });  
    }

    var editPersonalInfo_2 = function() {
        $( "#edit_personal_info_2" ).validate({
            // define validation rules
            rules: {
                username: {
                    required: true,
                    minlength: 8
                },
                newpasswd: {
                    minlength: 8
                },
                retypepasswd: {
                    equalTo: '[name="newpasswd"]'
                },
                usertype: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                mApp.scrollTo("#edit_personal_info_2");

                swal({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            submitHandler: function (form) {

                var user_id = $("input[name='user_id']").val();
                var username = $("input[name='username']").val();
                var password = $("input[name='password']").val();
                var usertype = $(':checkbox:checked').val();
                var status = $("select[name='status']").val();

                var options = { 
                    url:   $(this).attr('action'),
                    type: 'POST',
                    dataType: "json",
                    data: {user_id:user_id, username:username, password:password, usertype:usertype, status:status},
                    success:  function(response) { 
                        if(response.status === true)
                        {
                           swal({
                                "title": "", 
                                "text": "Successfully updated user.", 
                                "type": "success",
                                "timer": 1500,
                                "showConfirmButton": false

                            });

                        //    window.setTimeout(function(){
                        //     document.location.href = response.redirect;

                        // }, 1800);


                        }
                    },
                    error:  function() { 
                        alert('error'); 
                    },
                }; 
              $(form).ajaxSubmit(options);
            }
        });  
    }

    return {
        // public functions
        init: function() {
            addUser(); 
            editPersonalInfo();
            editPersonalInfo_2();
        }
    };
}();

jQuery(document).ready(function() {    
    FormValidation.init();



$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
}, "Only alphabetical characters"); 


});