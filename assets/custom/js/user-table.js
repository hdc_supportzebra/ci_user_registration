var DatatableRecordSelectionDemo = function() {
    //== Private functions
    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: 'get_users',
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },
        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
            height: 550, // datatable's body's fixed height
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        // columns definition
        columns: [{
            field: 'uid',
            title: '#',
            sortable: false,
            width: 40,
            textAlign: 'center',
            selector: {
                class: 'm-checkbox--solid m-checkbox--brand'
            },
        }, {
            field: 'user_id',
            title: 'ID',
            width: 40,
            template: '{{uid}}',
        }, {
            field: 'firstname',
            title: 'First Name',
            width: 100,
        }, {
            field: 'lastname',
            title: 'Last Name',
        }, {
            field: 'user_type',
            title: 'User Type',
        }, {
            field: 'Status',
            title: 'Status',
            template: function(row) {
                var status = {
                    0: {
                        'title': 'Inactive',
                        'class': 'm-badge--metal'
                    },
                    1: {
                        'title': 'Active',
                        'class': ' m-badge--brand'
                    },
                };
                return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
            },
        }, {
            field: 'Actions',
            width: 110,
            title: 'Actions',
            sortable: false,
            overflow: 'visible',
            template: function(row, index, datatable) {
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

                var statusText = (row.Status == 1 ? '<span class="m-badge m-badge--metal m-badge--wide">Inactive</span>' : '<span class="m-badge  m-badge--brand m-badge--wide">Active</span>');

                return '\
				<div class="dropdown ' + dropup + '">\
					<a href="#" id="user-action-btn" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
						<i class="la la-ellipsis-h"></i>\
					</a>\
					<div class="dropdown-menu dropdown-menu-right">\
						<a class="dropdown-item"href="users/view/' + row.uid + '"><i class="la la-eye"></i> View</a>\
						<a class="dropdown-item" href="users/edit/' + row.uid + '"><i class="la la-edit"></i> Edit Details</a>\
						<a class="dropdown-item" onClick="updateStatus(' + row.uid + ',' + row.Status + ')"><i class="la la-leaf"></i> Set to <b>' + statusText + '</b></a>\
						<a class="dropdown-item" onClick="deleteUser(' + row.uid + ')"><i class="la la-trash"></i> Delete</a>\
					</div>\
				</div>\
				';
            },
        }],
    };
    // basic demo
    var localSelectorDemo = function() {
        options.search = {
            input: $('#generalSearch'),
        };
        var datatable = $('#local_record_selection').mDatatable(options);
        $('#m_form_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#m_form_status').selectpicker();
        datatable.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function(e) {
            var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
            var count = checkedNodes.length;


            $('#m_datatable_selected_number').html(count);
            if (count > 0) {
                $('#m_datatable_group_action_form').collapse('show');

                $('.dropdown').each(function(i, obj) {
                    $(this).find('#user-action-btn').addClass('disabled');
                });

            } else {
                $('#m_datatable_group_action_form').collapse('hide');

                $('.dropdown').each(function(i, obj) {
                    $(this).find('#user-action-btn').removeClass('disabled');
                });

            }
        });
        $('#m_modal_fetch_id').on('show.bs.modal', function(e) {
            var ids = datatable.rows('.m-datatable__row--active').
            nodes().
            find('.m-checkbox--single > [type="checkbox"]').
            map(function(i, chk) {
                return $(chk).val();
            });
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.m_datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function(e) {
            $(e.target).find('.m_datatable_selected_ids').empty();
        });

    };
    return {
        // public functions
        init: function() {
            localSelectorDemo();
        },
    };
}();
jQuery(document).ready(function() {
    DatatableRecordSelectionDemo.init();

});

function getCheckedIds() {

    var datatable = $('#local_record_selection').mDatatable(DatatableRecordSelectionDemo.options);
    var ids = datatable.rows('.m-datatable__row--active')
        .nodes()
        .find('.m-checkbox--single > [type="checkbox"]')
        .map(function(i, chk) {
            return $(chk).val();
        });

	var uids = [];
	var c = document.createDocumentFragment();
	for (var i = 0; i < ids.length; i++) {
		var li = document.createElement('li');
		li.setAttribute('data-id', ids[i]);
		li.innerHTML = 'Selected record ID: ' + ids[i];
		c.appendChild(li);

        uids[i] = ids[i];
    }

    return uids;

}

function deleteUser(uid) {
	if(uid == undefined)
	{
		uid = getCheckedIds();
	}

    swal({
        title: 'Warning!',
        text: "Are you sure you want to permanently delete this user?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
        if (result.value) {

            $.ajax({
                // url: "delete_user/" + uid,
                url: "user_panel/delete_user" ,
                type: 'POST',
                dataType: "json",
                data: {
                    user_id: uid
                },
                success: function(data) {
                	console.log(data.value);
                    if (data.status === true) {
                        swal({
                            "title": "",
                            "text": "Successfully deleted user.",
                            "type": "success",
                            "timer": 1500,
                            "showConfirmButton": false

                        });
                        $('.m_datatable').mDatatable('reload');
                    } else {

                        swal({
                            "title": "",
                            "text": "There was an error deleting user.",
                            "type": "error",
                            "timer": 2000,
                            "showConfirmButton": false
                        });
                    }
                },
                error: function() {
                    alert('error');
                }
            });
        }
    });
}

function updateStatus(uid, status) {
	if(uid == 'bulk_update')
	{
		uid = getCheckedIds();
	}

    swal({
        text: "Are you sure you want to update the user's status?",
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then(function(result) {
        if (result.value) {

            $.ajax({
                url: "user_panel/update_status",
                type: 'POST',
                dataType: "json",
                data: {
                    user_id: uid,
                    status: status
                },
                success: function(data) {

                    console.log(data.value);
                    if (data.status === true) {
                        swal({
                            "title": "",
                            "text": "Successfully updated status.",
                            "type": "success",
                            "timer": 1500,
                            "showConfirmButton": false

                        });
                        $('.m_datatable').mDatatable('reload');
                    } else {

                        swal({
                            "title": "",
                            "text": "There was an error updating the status.",
                            "type": "error",
                            "timer": 2000,
                            "showConfirmButton": false
                        });
                    }
                },
                error: function() {
                    swal({
                            "title": "",
                            "text": "Error updating the status.",
                            "type": "error",
                            "timer": 2000,
                            "showConfirmButton": false
                        });
                }
            });
        }
    });
}