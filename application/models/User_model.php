<?php

class User_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function is_user($data)
	{
		$this->db->where(['AND', 
			'username' => $data['username'],
			'password' => $data['password']
		]);

		$query = $this->db->get('tbl_user');

		if ( $query->num_rows() != 0 )
		{
			return true;
		}
	}
	
	public function get_user($data)
	{
		$this->db->select('user.user_id as user_id, user.fname as firstname, user.lname as lastname, user.username as username, user.isActive as status, userrole.role_id as role_id, userrole.role_type as user_type')
			->from('tbl_user as user');

			if(isset($data['session']) && $data['session'])
			{
				$this->db->where(['AND', 
					'username' => $data['username'],
					'password' => $data['password']
				]);
			}

			else if(isset($data['user_id']) && !empty($data['user_id']))
			{
				$this->db->where('user.user_id', $data['user_id']);
			}

			$this->db->join('tbl_user_role as userrole', 'userrole.role_id = user.role_id');
			$query = $this->db->get();

		return $query->result();
	}

	public function get_user_types()
	{
		$query = $this->db->get('tbl_user_role');

		return $query->result();
	}

	public function get_user_list()
	{
		$query = $this->db->select('user.user_id as user_id, user.user_id as uid, user.fname as firstname, user.lname as lastname, user.username as username, user.isActive as Status, userrole.role_id as role_id, userrole.role_type as user_type')
			->from('tbl_user as user')
			->join('tbl_user_role as userrole', 'userrole.role_id = user.role_id')
			->get();

		return $query->result();
	}

	public function get_user_details($data)
	{
		$query = $this->db->select('user.user_id as user_id, user.fname as firstname, user.lname as lastname, user.username as username, user.isActive as status, userrole.role_type as type')
			->from('tbl_user as user')
			->where('user_id', $data['user_id'])
			->join('tbl_user_role as userrole', 'userrole.role_id = user.role_id')
			->get();

		return $query->result();
	}

	public function insert($data)
	{
		$result = $this->db->insert('tbl_user', $data);

		return $result;
	}

	public function edit($data)
	{
		$this->db->where('user_id', $this->input->post('user_id'));
		$result = $this->db->update('tbl_user', $data);

		return $result;
	}
}