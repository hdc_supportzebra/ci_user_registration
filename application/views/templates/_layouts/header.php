<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
    <!-- begin::Head -->
    
<!-- Mirrored from keenthemes.com/metronic/preview/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Mar 2018 12:32:50 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8" />
        
        <title><?php if (isset($title)){ echo $title; } else { echo 'User Registration'; }; ?></title>
        <meta name="description" content="Latest updates and statistic charts"> 

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       

        <!--begin::Web font -->
        <script src="http://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->

        <!--begin::Base Styles -->  

                 
        <!--begin::Page Vendors --> 
                <link href="<?php echo base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css'); ?>" rel="stylesheet" type="text/css" />
                <!--end::Page Vendors -->
         

				<link href="<?php echo base_url('assets/vendors/base/vendors.bundle.css'); ?>" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url('assets/demo/default/base/style.bundle.css'); ?>" rel="stylesheet" type="text/css" />
		        <!--end::Base Styles -->

        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>" /> 
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37564768-1', 'auto');
  ga('send', 'pageview');

</script>

<style>

  label {
    font-weight: 400;
    color: #444;
  }
  .login-form {
      position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    /* border: 1px dashed deeppink; */
    width: 500px;
    padding: 20px;
    background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.05);
}
.alert-danger:not(.m-alert--outline) {
  padding-bottom: 0;
}

.m-aside-menu .m-menu__nav {
    padding-top: 0 !important;
}
.m-aside-menu .m-menu__nav > .m-menu__section {
    margin-top: 5px !important;
}
.m-subheader {
    padding-top: 15px !important;
}
.m-body .m-content {
    padding-top: 15px !important;
}
.checkbox-select {
    padding: 0 30px !important;
} 

.m-datatable__row {
  border-bottom: 1px solid #eee;
}
.m-nav__link-text strong {
  color: #444;
}
.m-form .m-form__seperator.m-form__seperator--space-2x {
  margin: 0 !important;
}
</style>
 <!-- <script src="<?php// echo base_url('assets/vendors/base/js/jquery.js'); ?>"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
</head>
    <!-- end::Head -->

    
    <!-- end::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

        
        