<?php include('top_bar.php'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<?php include('side_bar.php'); ?>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">Add User</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="#" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__item">
							<a href="<?php echo base_url('users');?>" class="m-nav__link">
								<span class="m-nav__link-text">Users</span>
							</a>
						</li>
						<li class="m-nav__separator">/</li>
						<li class="m-nav__item">
							<!-- <a href="" class="m-nav__link"> -->
							<span class="m-nav__link-text"><strong>Add User</strong></span>
							<!-- </a> -->
						</li>
						
					</ul>
				</div>
				<div>
					
				</div>
			</div>
		</div>
		<div class="m-content">
			<!--begin::Portlet-->
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
							User Registration Form
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="add_user_form" method="POST" action="<?php echo base_url('add_success');?>">
					<div class="m-portlet__body">
						<div class="m-form__content">
							<div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
								<div class="m-alert__icon">
									<i class="la la-warning"></i>
								</div>
								<div class="m-alert__text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="m-alert__close">
									<button type="button" class="close" data-close="alert" aria-label="Close">
									</button>
								</div>
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">First Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control m-input" name="firstname" placeholder="" />
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">Last Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control m-input" name="lastname" placeholder="" />
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">Username *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control m-input" name="username" placeholder="" />
								<span class="m-form__help">Please enter a username with atleast 8 characters.</span>
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="password" class="form-control m-input" name="password" placeholder="" />
								<span class="m-form__help">Please enter a username with atleast 8 characters.</span>
							</div>
						</div>
						
						<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
						
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">User Permission *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="m-checkbox-list">
									<label class="m-checkbox">
										<input type="checkbox" name="usertype" value="5"> User
										<span></span>
									</label>
									<label class="m-checkbox">
										<input type="checkbox" name="usertype" value="6"> Admin
										<span></span>
									</label>
									<label class="m-checkbox">
										<input type="checkbox" name="usertype" value="7"> Super Admin
										<span></span>
									</label>
								</div>
								<span class="m-form__help">Please select user permission.</span>
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-3 col-sm-12">Status *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="m-radio-inline">
									<label class="m-radio">
										<input type="checkbox" name="status" value="1"> Active
										<span></span>
									</label>
									<label class="m-radio">
										<input type="checkbox" name="status" value="0"> Inactive
										<span></span>
									</label>
									
								</div>
								<span class="m-form__help">Please select status of user.</span>
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" class="btn btn-accent">Register</button>
									<button type="reset" class="btn btn-secondary">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
		
	</div>
	
</div>
<!-- end:: Body -->
<script src="<?php echo base_url('assets/custom/js/form-validation.js');?>" type="text/javascript"></script>