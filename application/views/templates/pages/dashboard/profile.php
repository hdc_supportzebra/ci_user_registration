<?php include('top_bar.php'); ?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('side_bar.php'); ?>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">My Profile</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-xl-3 col-lg-4">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img src="assets/images/user.png" width="80" alt=""/>
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name"><?php  echo $current_user->firstname . ' '. $current_user->lastname; ?></span>
									<a href="#" class="m-card-profile__email m-link"><?php echo $current_user->user_type; ?></a>
								</div>
							</div>
							<div class="m-portlet__body-separator"></div>
						</div>
					</div>
				</div>
				<div class="col-xl-9 col-lg-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											Update Profile
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
											Settings
										</a>
									</li>
								</ul>
							</div>
							
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form class="m-form m-form--fit m-form--label-align-right">
									<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<div class="col-10 ml-auto">
												<h3 class="m-form__section">Personal Details</h3>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Full Name</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="<?php echo $current_user->firstname . ' '. $current_user->lastname; ?>" >
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Position/Title</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Phone No.</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Home Address</label>
											<div class="col-7">
												<textarea class="form-control m-input" type="text" value="L-12-20 Vertex, Cybersquare"></textarea>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Email Address</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="">
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2">
												</div>
												<div class="col-7">
													<button type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane" id="m_user_profile_tab_2">
								<form class="m-form m-form--fit m-form--label-align-right">
									<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<div class="col-10 ml-auto">
												<h3 class="m-form__section">1. Change Password</h3>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">New Password</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">Re-type new password</label>
											<div class="col-7">
												<input class="form-control m-input" type="text" value="">
											</div>
										</div>
									</div>
									<div class="m-portlet__body-separator"></div>
									<div class="m-portlet__body">
										<div class="form-group m-form__group row">
											<div class="col-10 ml-auto">
												<h3 class="m-form__section">2. User Permission</h3>
											</div>
										</div>
										<div class="form-group m-form__group row checkbox-select">
											<label for="example-text-input" class="col-2 col-form-label"></label>
											<div class="col-7">
												<div class="row">
													<input type="checkbox" name="user_type" class="form-check-input" id="exampleCheck1"> User
												</div>
												
											</div>
										</div>
										<div class="form-group m-form__group row checkbox-select">
											<label for="example-text-input" class="col-2 col-form-label"></label>
											<div class="col-7">
												<div class="row">
													<input type="checkbox" name="user_type" class="form-check-input" id="exampleCheck1"> Admin
												</div>
												
											</div>
										</div>
										<div class="form-group m-form__group row checkbox-select">
											<label for="example-text-input" class="col-2 col-form-label"></label>
											<div class="col-7">
												<div class="row">
													<input type="checkbox" name="user_type" class="form-check-input" id="exampleCheck1"> Super Admin
												</div>
												
											</div>
										</div>
									</div>
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2">
												</div>
												<div class="col-7">
													<button type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<script type="text/javascript">
	$("input:checkbox").click(function() {
	if ($(this).is(":checked")) {
	var group = "input:checkbox[name='" + $(this).attr("name") + "']";
	$(group).prop("checked", false);
	$(this).prop("checked", true);
	} else {
	$(this).prop("checked", false);
	}
	});
</script>