<?php include('top_bar.php'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<?php include('side_bar.php'); ?>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">Edit User</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="<?php echo base_url('dashboard');?>" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__item">
							<a href="<?php echo base_url('users');?>" class="m-nav__link">
								<span class="m-nav__link-text">Users</span>
							</a>
						</li>
						<li class="m-nav__separator">/</li>
						<li class="m-nav__item">
							<!-- <a href="" class="m-nav__link"> -->
							<span class="m-nav__link-text"><strong>Edit User</strong></span>
							<!-- </a> -->
						</li>
						
					</ul>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="m-portlet">
				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										Update Profile
									</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
										Settings
									</a>
								</li>
							</ul>
						</div>
						
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							<!--begin::Form-->
							<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="edit_personal_info" action="<?php echo base_url('edit_success/'.$user->user_id);?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">Personal Information</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">First Name </label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="firstname" value="<?php echo $user->firstname; ?>" />
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Last Name </label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="lastname" value="<?php echo $user->lastname; ?>" />
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-accent">Save changes</button>
												<button type="reset" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
							<!--end::Form-->
						</div>
						<div class="tab-pane" id="m_user_profile_tab_2">
							<!--begin::Form-->
							<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="edit_personal_info_2" action="<?php echo base_url('edit_success/'.$user->user_id);?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">Update Credentials</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Username </label>
										<div class="col-9">
											<input type="hidden" name="user_id" value="<?php echo $user->user_id;?>">
											<input type="text" class="form-control m-input" name="username" value="<?php echo $user->username; ?>" />
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">New Password </label>
										<div class="col-9">
											<input type="password" class="form-control m-input" name="newpasswd" placeholder="" />
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Re-type new password </label>
										<div class="col-9">
											<input type="password" class="form-control m-input" name="retypepasswd" placeholder="" />
										</div>
									</div>
								</div>
								<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">Permission & Status</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">User Permission</label>
										<div class="col-9">
											<div class="m-checkbox-list">
												<label class="m-checkbox">
											<input type="hidden" name="user_id" value="<?php echo $user->user_id;?>">
													<input type="checkbox" name="usertype"  value="5" <?php if($user->role_id == '5') echo 'checked'; else '';?>> User
													<span></span>
												</label>
												<label class="m-checkbox">
													<input type="checkbox" name="usertype" value="6" <?php if($user->role_id == '6') echo 'checked'; else '';?>> Admin
													<span></span>
												</label>
												<label class="m-checkbox">
													<input type="checkbox" name="usertype" value="7" <?php if($user->role_id == '7') echo 'checked'; else '';?>> Super Admin
													<span></span>
												</label>
											</div>
											<span class="m-form__help">Please select at lease 1 and maximum 2 options</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Status </label>
										<div class="col-9">
											<div class="m-radio-inline">
												<label class="m-radio">
													<input type="checkbox" name="status" value="1" <?php if($user->status == '1') echo 'checked'; else '';?>> Active
													<span></span>
												</label>
												<label class="m-radio">
													<input type="checkbox" name="status" value="0"  <?php if($user->status == '0') echo 'checked'; else '';?>>Inactive
													<span></span>
												</label>
												
											</div>
										</div>
									</div>
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-accent">Save changes</button>
												<button type="reset" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
							<!--end::Form-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<script src="<?php echo base_url('assets/custom/js/form-validation.js');?>" type="text/javascript"></script>