<?php include('top_bar.php'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<?php include('side_bar.php'); ?>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">View User</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="<?php echo base_url('dashboard');?>" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__item">
							<a href="<?php echo base_url('users');?>" class="m-nav__link">
								<span class="m-nav__link-text">Users</span>
							</a>
						</li>
						<li class="m-nav__separator">/</li>
						<li class="m-nav__item">
							<!-- <a href="" class="m-nav__link"> -->
							<span class="m-nav__link-text"><strong>View User Details</strong></span>
							<!-- </a> -->
						</li>
						
					</ul>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="m-portlet">
				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										<?php echo $user->firstname.' '. $user->lastname; ?>
									</a>
								</li>
								
							</ul>
						</div>
						
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							<!--begin::Form-->
							<form class="m-form m-form--state m-form--fit m-form--label-align-right">
								
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">Personal Information</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Fullname </label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="username" value="<?php echo $user->firstname.' '. $user->lastname; ?>" readonly />
										</div>
									</div>
								</div>
								<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">User Credential</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Username </label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="username" value="<?php echo $user->username; ?>" readonly/>
										</div>
									</div>
								</div>
								<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">Permission & Status</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">User Permission</label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="username" value="<?php echo $user->user_type; ?>" readonly/>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">Status </label>
										<div class="col-9">
											<input type="text" class="form-control m-input" name="username" value="<?php if($user->status == 1) echo 'Active'; else { echo 'Inactive'; } ; ?>" readonly/>
										</div>
									</div>
									
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Body -->