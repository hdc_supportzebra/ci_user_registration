<?php include('top_bar.php'); ?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	
	<?php include('side_bar.php'); ?>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">Users</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="#" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__item">
							<a href="#" class="m-nav__link">
								<span class="m-nav__link-text">Users</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- END: Subheader -->

		<div class="m-content">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<!--begin: Search Form -->
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="m-form__group m-form__group--inline">
											<div class="m-form__label">
												<label>Status:</label>
											</div>
											<div class="m-form__control">
												<select class="form-control m-bootstrap-select m-bootstrap-select--solid" id="m_form_status">
													<option value="">All</option>
													<option value="0">Inactive</option>
													<option value="1">Active</option>
												</select>
											</div>
										</div>
										<div class="d-md-none m--margin-bottom-10"></div>
									</div>
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
												<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="<?php echo base_url('users/add-user');?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
									<span>
										<i class="la la-user-plus"></i>
										<span>New User</span>
									</span>
								</a>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<!--end: Search Form -->
					<!--begin: Selected Rows Group Action Form -->
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
						<div class="row align-items-center">
							<div class="col-xl-12">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label m-form__label-no-wrap">
										<label class="m--font-bold m--font-danger-">Selected
											<span id="m_datatable_selected_number">0</span> records:</label>
										</div>
										<div class="m-form__control">
											<div class="btn-toolbar">
												<div class="dropdown">
													<button type="button" class="btn btn-accent btn-sm dropdown-toggle" data-toggle="dropdown">
													Update status
													</button>
													<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
														<a class="dropdown-item" href="#" onClick="updateStatus('bulk_update', '0')">Inactive</a>
														<a class="dropdown-item" href="#" onClick="updateStatus('bulk_update', '1')">Active</a>
													</div>
												</div>
												&nbsp;&nbsp;&nbsp;
												<button class="btn btn-sm btn-danger" data-target="#bulk_delete" type="button" onClick="deleteUser()">Delete All</button>
												&nbsp;&nbsp;&nbsp;
												<button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#m_modal_fetch_id">Fetch Selected Records</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end: Selected Rows Group Action Form -->
						<!--begin: Datatable -->
						<div class="m_datatable" id="local_record_selection"></div>
						<!--end: Datatable -->
						<!--begin::Modal-->
						<div class="modal fade" id="m_modal_fetch_id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="200">
										<ul class="m_datatable_selected_ids"></ul>
									</div>
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->
<!-- end:: Body -->
<script src="<?php echo base_url('assets/custom/js/user-table.js');?>" type="text/javascript"></script>