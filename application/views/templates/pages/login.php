<div class="container">
	<div class="login-form">
	<h1>Login</h1>
		<?php echo validation_errors(); ?>
		<div class="alert alert-danger print-error-msg" style="display:none">
    	</div>
		<form>
			<div class="form-group">
				<label for="exampleInputEmail1">Username</label>
				<input type="username" class="form-control" id="username" name="username" placeholder="Enter username">
				<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			</div>

			<button type="submit" class="btn btn-primary btn-submit">Submit</button>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
	    $(".btn-submit").click(function(e){
	    	e.preventDefault();

	    	var username = $("input[name='username']").val();
			var password = $("input[name='password']").val();

	        $.ajax({
	            url: "user_authentication/login",
	            type:'POST',
	            dataType: "json",
	            data: {username:username, password:password},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	$(".print-error-msg").css('display','none');
	                	 if( data.status === true )
           		 		document.location.href = data.redirect;
	                }else{
						$(".print-error-msg").css('display','block');
	                	$(".print-error-msg").html(data.error);
	                }
	            }
	        });
	    });
	});

</script>