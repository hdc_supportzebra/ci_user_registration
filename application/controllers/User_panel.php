<?php

class User_panel extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('session');
		
	}


	public function dashboard()
	{
		$criteria = [
			'user_session' => true,
			'title' => 'Dashboard',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/index',
				'templates/_layouts/footer'
			],
		];

		$this->_get_page($criteria);
	}

	public function users_page()
	{
		$criteria = [
			'title' => 'Users',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/user_list',
				'templates/_layouts/footer'
			],
		];

		$this->_get_page($criteria);
	}

	public function profile_page()
	{
		$criteria = [
			'title' => 'My Profile | Settings',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/profile',
				'templates/_layouts/footer'
			],
		];

		$this->_get_page($criteria);
	}

	public function add_user_page()
	{
		$criteria = [
			'title' => 'Add New User',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/add_user',
				'templates/_layouts/footer'
			],
		];

		$this->_get_page($criteria);
	}

	public function edit_user_page($id=null)
	{
		$user['user_id'] = $id;

		$data = $this->user_model->get_user($user);
	
		$criteria = [
			'title' => 'Edit User',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/edit_user',
				'templates/_layouts/footer'
			],
			'data' => [
				'user' => $data[0]
			]
		];

		$this->_get_page($criteria);
	}

	public function view_user_page($id=null)
	{
		$user['user_id'] = $id;

		$data = $this->user_model->get_user($user);
	
		$criteria = [
			'title' => 'View User Details',
			'templates' => [
				'templates/_layouts/header',
				'templates/pages/dashboard/view_user',
				'templates/_layouts/footer'
			],
			'data' => [
				'user' => $data[0]
			]
		];

		$this->_get_page($criteria);
	}

	public function add_success()
	{
		$data = [
			'fname' => $this->input->post('firstname'),
			'lname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'isActive' => $this->input->post('status'),
			'role_id' => $this->input->post('usertype'),
		];

		$result = $this->user_model->insert($data);

		if ($result)
		{
			echo json_encode(array('status'=>true, 'redirect'=> base_url().'users'));
		}
	}

	public function get_user_list()
	{
		$data['data'] = $this->user_model->get_user_list();
		
		echo json_encode($data); 
	}

	public function edit_success( $user_id = null)
	{

		if(!empty($this->input->post('firstname')))
		{
			$data['fname'] = $this->input->post('firstname');
		}
		if(!empty($this->input->post('lastname')))
		{
			$data['lname'] = $this->input->post('lastname');
		}
		if(!empty($this->input->post('username')))
		{
			$data['username'] = $this->input->post('username');
		}
		if(!empty($this->input->post('newpasswd')))
		{
			$data['password'] = $this->input->post('newpasswd');
		}
		if(!empty($this->input->post('status')))
		{
			$data['isActive'] = $this->input->post('status');
		}
		if(!empty($this->input->post('usertype')))
		{
			$data['role_id'] = $this->input->post('usertype');
		}

		$edit_user = $this->user_model->edit($data);

		if($edit_user)
		{
			echo json_encode(array('status'=>true, 'redirect'=> base_url().'users/edit/'.$user_id));
		}
	}

	public function update_status()
	{
		$user_id = $this->input->post('user_id');
		$status = $this->input->post('status');

		if(!is_array($user_id))
		{
			$status = ($status == '1' ? 0 : 1);
			$this->db->set('isActive', $status);
			$this->db->where('user_id', $user_id);

			$update = $this->db->update('tbl_user');
		}

		else
		{

			foreach($user_id as $id => $value)
			{
			// 	echo  "<br />";
				$data[] = ['user_id' => $value, 'isActive' => $status];
			}
	
			// echo "<pre>";
			// var_dump($data);
			// // $data = $uid;

			$update = $this->db->update_batch('tbl_user', $data, 'user_id');
		}
		
		if($update)
		{
			echo json_encode(['status' => true, 'value' => $status]);
		}


	}

	public function delete_user()
	{

		$id = $this->input->post('user_id');

		$this->db->where_in('user_id', $id);
		$delete = $this->db->delete('tbl_user');

		if($delete)
		{

			echo json_encode(array('status'=>true, 'value'=>$id));
		}
		else
		{
			echo json_encode(array('error'=>true, 'value'=>$id));
		}
	}

	// ------ PRIVATE METHODS

	private function _get_page($criteria)
	{
		$session = $this->_get_session();

		$data['current_user'] = $session[0];

		$data['title'] = $criteria['title'];

		if(isset($criteria['data']) && $criteria['data'] !== NULL)
		{
			$data = array_merge_recursive($data, $criteria['data']);
		}

		foreach($criteria['templates'] as $template)
		{
			$this->load->view($template, $data);
		}

	}

	private function _get_session()
	{
		$user = $this->session->userdata('loggedIn');
		
		$user['session'] = true;

		$has_session = $this->user_model->get_user($user);

		if ($has_session)
		{
			return $has_session;
		}
		else 
		{
			redirect(base_url(''));
		}
	}


}