<?php

class User_authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		
	}

	public function index()
	{
		$session = $this->get_session();

		if ($session)
		{
			redirect(base_url('dashboard'));
		}
		else
		{
			$this->load->view('templates/index');
		}
	}

	public function login()
	{
		$this->form_validation->set_rules('username', '<b>Username</b>', 'required');
		$this->form_validation->set_rules('password', '<b>Password</b>', 'required');
		
		if ( ! $this->form_validation->run() == FALSE )
		{
			$data = [
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password')
			];

			// Load user mdel
			$this->load->model('user_model');

			$isUser = $this->user_model->is_user($data);

			if ($isUser)
			{
				// Set user session
				$this->set_session(['loggedIn' => $data]);

				echo json_encode(array('status'=>true, 'redirect'=> base_url().'dashboard'));

				return true;
			}

			else
			{
				echo json_encode(['error' => '<p>Incorrect username/email.</p>',  'redirect'=> base_url()]);
			}
		}
		else
		{
	        if ( $errors = validation_errors() )
			{
				echo json_encode(['error' => $errors]);
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('loggedIn');
		$this->session->sess_destroy();
		redirect(base_url(''));
	}

	public function get_session()
	{
		return $this->session->userdata('loggedIn');
	}

	public function set_session($data = array())
	{
		$this->session->set_userdata($data);
	}
}